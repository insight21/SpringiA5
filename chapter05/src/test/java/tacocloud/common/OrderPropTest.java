package tacocloud.common;

import lombok.Data;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Component
@Data
@ConfigurationProperties(prefix = "taco.orders")
public class OrderPropTest {
    @Autowired
    private OrderProp prop;

    private int pageSize = 20;

    /**
     * bug 记录...
     * 由于配置了 tokenRepository.setCreateTableOnStartup(true) 每次启动都会创建表
     * 导致重复创建 因此只能设置一次 之后需要关掉
     * 于是粗暴的地把相关部分注释掉了...
     */
    @Test
    public void testPagesize() {
        System.out.println(pageSize);
        System.out.println(prop.getPageSize());
    }
}