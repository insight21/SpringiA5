package tacocloud.common;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 将属性抽取出来 放到属性配置类
 * 
 * @author insight 
 * @since 2021/7/7
 */
@Component
@Data
@ConfigurationProperties(prefix = "taco.orders")
public class OrderProp {
    private int pageSize = 20;
}