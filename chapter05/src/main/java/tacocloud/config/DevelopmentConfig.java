package tacocloud.config;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import tacocloud.data.IngredientRepository;
import tacocloud.data.UserRepository;
import tacocloud.entity.TacoIngredient;
import tacocloud.entity.TacoIngredient.Type;
import tacocloud.entity.User;

/**
 * 假设有需求：应用启动时加载嵌入式数据库和配料数据
 * 只在开发阶段使用
 * 使用 @Profile 来激活或不激活配置文件
 */
@Profile("!prod")
@Configuration
public class DevelopmentConfig {

  @Bean
  public CommandLineRunner dataLoader(IngredientRepository repo,
                                      UserRepository userRepo, PasswordEncoder encoder) { // user repo for ease of testing with a built-in user
    return args -> {
      repo.save(new TacoIngredient("FLTO", "Flour Tortilla", Type.WRAP));
      repo.save(new TacoIngredient("COTO", "Corn Tortilla", Type.WRAP));
      repo.save(new TacoIngredient("GRBF", "Ground Beef", Type.PROTEIN));
      repo.save(new TacoIngredient("CARN", "Carnitas", Type.PROTEIN));
      repo.save(new TacoIngredient("TMTO", "Diced Tomatoes", Type.VEGGIES));
      repo.save(new TacoIngredient("LETC", "Lettuce", Type.VEGGIES));
      repo.save(new TacoIngredient("CHED", "Cheddar", Type.CHEESE));
      repo.save(new TacoIngredient("JACK", "Monterrey Jack", Type.CHEESE));
      repo.save(new TacoIngredient("SLSA", "Salsa", Type.SAUCE));
      repo.save(new TacoIngredient("SRCR", "Sour Cream", Type.SAUCE));


      userRepo.save(new User("habuma", encoder.encode("password"),
          "Craig Walls", "123 North Street", "Cross Roads", "TX",
          "76227", "123-123-1234"));
    };
  }
  
}
