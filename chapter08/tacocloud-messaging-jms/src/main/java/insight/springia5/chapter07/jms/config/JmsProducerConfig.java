package insight.springia5.chapter07.jms.config;

import insight.springia5.chapter06.domain.Order;
import org.apache.activemq.artemis.jms.client.ActiveMQQueue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;

import javax.jms.Destination;
import java.util.HashMap;

/**
 * 配置 Destination & MessageConverter
 * 
 * @author insight
 * @since 2021/7/21
 */
@Configuration
public class JmsProducerConfig {
    @Bean
    public Destination destination() {
        return new ActiveMQQueue("tacocloud.order.queue");
    }
    
    @Bean
    public MessageConverter messageConverter() {
        MappingJackson2MessageConverter converter =
                new MappingJackson2MessageConverter();
        // 返回前需要设置消息类型
        // 默认情况下，它将会包含要转换的类型的全限定类名
        // 不灵活 需要接收端也包含一模一样的类型
        converter.setTypeIdPropertyName("_typeId");
        
        // 使用映射 不用发送全限定类型
        // 将一个合成的 order 类型 ID 映射为 Order 类
        // 将 order 映射为它自己能够理解的订单类型
        // 在接收端的订单可能位于不同的包 不同的类 甚至可以只包含 Order 属性的一个子集
        HashMap<String, Class<?>> typeMap = new HashMap<>(2);
        typeMap.put("order", Order.class);
        converter.setTypeIdMappings(typeMap);
        
        return converter;
    }
}