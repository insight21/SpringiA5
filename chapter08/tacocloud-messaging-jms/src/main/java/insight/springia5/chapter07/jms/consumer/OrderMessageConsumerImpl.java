package insight.springia5.chapter07.jms.consumer;

import insight.springia5.chapter06.domain.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.stereotype.Service;

/**
 * 拉取和推送两种模式
 * 
 * 拉取: 同步 会阻塞
 * 推送: 异步
 * 
 * 具体情况具体使用
 * 如果消息发送速度太快 监听可能就会过载
 * 
 * JmsTemplate 使用的是 拉取
 * 定义消息监听器 使用 推送
 * 
 * JmsTemplate:
 * receive() & receiveAndConvert()
 * 与 send() & sendAndConvert() 是对称的
 * 可以指定消息的目的地 来选择性接收
 * 
 * @author insight
 * @since 2021/7/21
 */
@Service
public class OrderMessageConsumerImpl implements OrderMessageConsumer {
    private JmsTemplate jmsTemplate;
    private MessageConverter converter;

    @Autowired
    public OrderMessageConsumerImpl(JmsTemplate jmsTemplate,
                                    MessageConverter converter) {
        this.jmsTemplate = jmsTemplate;
        this.converter = converter;
    }

    /**
     * 使用 receive() 并转换
     */
    /*@Override
    public Order receive() {
        Message message = jmsTemplate.receive("tacocloud.order.queue");
        try {
            return (Order) converter.fromMessage(message);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }*/

    /**
     * 使用 receiveAndConvert()
     */
    @Override
    public Order receive() {
        return (Order) jmsTemplate
                .receiveAndConvert("tacocloud.order.queue");
    }
}