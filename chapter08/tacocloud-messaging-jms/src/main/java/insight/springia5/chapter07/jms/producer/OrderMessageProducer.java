package insight.springia5.chapter07.jms.producer;

import insight.springia5.chapter06.domain.Order;

/**
 * @author insight
 * @since 2021/7/21
 */
public interface OrderMessageProducer {

    /**
     * 发送订单
     *
     * @param order 订单
     */
    void sendOrder(Order order);

}