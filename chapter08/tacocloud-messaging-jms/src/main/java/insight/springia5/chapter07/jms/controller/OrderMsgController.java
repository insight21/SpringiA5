package insight.springia5.chapter07.jms.controller;

import insight.springia5.chapter06.domain.Order;
import insight.springia5.chapter07.jms.consumer.OrderMessageConsumer;
import insight.springia5.chapter07.jms.producer.OrderMessageProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * 编写控制器 测试一下 JMS
 * 
 * @author insight
 * @since 2021/7/21
 */
@RestController
@RequestMapping(path = "/orderMsg",
        produces = "application/json")
public class OrderMsgController {
    private OrderMessageProducer producer;
    private OrderMessageConsumer consumer;

    @Autowired
    public OrderMsgController(OrderMessageProducer producer,
                              OrderMessageConsumer consumer) {
        this.producer = producer;
        this.consumer = consumer;
    }
    
    @PostMapping(consumes = "application/json")
    public ResponseEntity<String> publish(@RequestBody Order order) {
        producer.sendOrder(order);
        return ResponseEntity.ok("Publish Successed!");
    }
    
    @GetMapping
    public ResponseEntity<Order> pull() {
        Order order = consumer.receive();
        return ResponseEntity.ok(order);
    }
}