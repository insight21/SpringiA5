package insight.springia5.chapter07.jms.consumer;

import insight.springia5.chapter06.domain.Order;

/**
 * 
 * 
 * @author insight 
 * @since 2021/7/21
 */
public interface OrderMessageConsumer {
    /**
     * 接受消息
     *
     * @return {@link Order}
     */
    Order receive();
}