package insight.springia5.chapter07.jms.consumer;

import insight.springia5.chapter06.domain.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * 使用注解实现消息监听
 * 
 * @author insight
 * @since 2021/7/21
 */
@Component
@Slf4j
public class OrderMessageListener {
    /**
     * 这里还大意了 没想到 artemis 需要本地安装启动的...
     * 书上没说 一直报 Failed to create session factory
     * debug 了半天...
     */
    @JmsListener(destination = "tacocloud.order.queue")
    public void receiveOrder(Order order) {
      log.info("Receive Order By Listener: " + order);  
    }

    /*@JmsListener(destination = "tacocloud.test.queue")
    public void receive(String text) {
        log.info("Receive Order By Listener: " + text);
    }*/
}