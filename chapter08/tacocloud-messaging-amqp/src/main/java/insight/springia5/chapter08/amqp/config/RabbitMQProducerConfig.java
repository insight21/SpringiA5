package insight.springia5.chapter08.amqp.config;

import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 配置 MessageConverter & Exchange & Queue & Binding
 * 解释: RabbitMQ 需要手动创建交换机 队列
 * 还需要将两者绑定起来 书上什么都没讲 又弄了半天...
 * 
 * @author insight
 * @since 2021/7/21
 */
@Configuration
public class RabbitMQProducerConfig {
    
    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }
    
    /*@Bean
    public Queue queue() {
        return new Queue("tacocloud.orders");
    }*/
    
    /*@Bean
    public Exchange exchange() {
        return new DirectExchange("tacocloud.orders");
    }*/

    /*@Bean
    public Binding binding(Exchange exchange, Queue queue) {
        return new Binding(queue.getName(), Binding.DestinationType.QUEUE,
                exchange.getName(), exchange.getName(), null);
    }*/
}