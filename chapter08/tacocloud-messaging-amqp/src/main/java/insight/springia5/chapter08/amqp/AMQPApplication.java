package insight.springia5.chapter08.amqp;

import insight.springia5.chapter06.domain.Order;
import insight.springia5.chapter08.amqp.producer.OrderMsgProducer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * 
 * 
 * @author insight
 * @since 2021/7/22
 */
@Slf4j
@SpringBootApplication
public class AMQPApplication {
    public static void main(String[] args) {
        SpringApplication.run(AMQPApplication.class, args);
    }
    
    @Autowired
    private OrderMsgProducer producer;
    
    /**
     * 测试 RabbitMQ
     */
    @Bean
    public CommandLineRunner send() {
        return args -> {
            producer.sendOrder(new Order());
            log.info("Publish Succeeed!");
        };
    }
}