package insight.springia5.chapter08.amqp.producer;

import insight.springia5.chapter06.domain.Order;

/**
 * 
 * 
 * @author insight 
 * @since 2021/7/22
 */
public interface OrderMsgProducer {
    /**
     * 发送订单
     *
     * @param order 订单
     */
    void sendOrder(Order order);
}