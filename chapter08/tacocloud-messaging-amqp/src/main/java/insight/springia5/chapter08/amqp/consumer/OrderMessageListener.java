package insight.springia5.chapter08.amqp.consumer;

import insight.springia5.chapter06.domain.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 使用注解实现消息监听
 * 
 * @author insight
 * @since 2021/7/22
 */
@Component
@Slf4j
public class OrderMessageListener {
    /*@RabbitListener(queues = "tacocloud.orders")
    public void receiveOrder(Order order) {
      log.info("Receive Order By Listener: " + order);  
    }*/

    /**
     * 自动创建队列
     */
    /*@RabbitListener(queuesToDeclare = @Queue("tacocloud.orders"))
    public void receiveOrder(Order order) {
        log.info("Receive Order By Queue-Auto-Declare Listener: " + order);
    }*/

    /**
     * 自动创建队列 exchange 并绑定
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue("tacocloud.orders"),
            exchange = @Exchange("tacocloud.orders")
    ))
    public void receiveOrder(Order order) {
        log.info("Receive Order By Binding-Auto-Declare Listener: " + order);
    }

    /**
     * 自动创建队列 exchange key 并绑定
     */
    /*@RabbitListener(bindings = @QueueBinding(
            value = @Queue("tacocloud.orders"),
            exchange = @Exchange("tacocloud.orders"),
            key = "tacocloud.orders"
    ))
    public void receiveOrder(Order order) {
        log.info("Receive Order By Binding-Auto-Declare Listener: " + order);
    }*/
}