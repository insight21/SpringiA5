package insight.springia5.chapter08.amqp.consumer;

import insight.springia5.chapter06.domain.Order;

/**
 * 
 * 
 * @author insight 
 * @since 2021/7/22
 */
public interface OrderMsgConsumer {
    /**
     * 接受消息
     *
     * @return {@link Order}
     */
    Order receive();
}