# AMQP 协议学习
> 在学习集成 RabbitMQ 之前, 先来学学它的协议 -- AMQP

### AMQP 是什么

AMQP (Advanced Message Queuing Protocol，高级消息队列协议)     
是一个进程间传递**异步消息**的**网络协议**

### 为什么使用 AMQP

个人理解: 解耦     

回顾 JMS, 消息, 发送者和接收者直接依赖于目的地对象 (Destination), 高耦合, 独立性差, 不利于后续修改和组合使用     
而 AMQP, 使用 `Exchange` 和 `routing key` 来发送接收消息, 这样消息就与接收者要监听的队列解耦.      

另外, AMQP 基于网络, 这意味着发布者，消费者，消息代理 可以分别存在于不同的设备上

### 工作过程

发送者发送消息到交换机 (Exchange), 交换机根据具体的规则, 把消息分发到队列

![image-20210722134254323](AMQP.png)

### 新元素

- Broker: 消息代理, 消息中间件, 常见的有 `RabbitMQ`
- Routing Key: 路由键, 由消息和队列携带
- Exchange: 交换机, 消息与队列的桥梁, 由它决定消息分发到哪个队列, 消息中会包含交换机的类型
- Binding Key: 绑定键, 由队列携带

### Exchange

当消息抵达消息代理 (Broker) 的时候，它会进入为其设置的 `Exchange` 上。`Exchange` 负责将它路由到一个或多个队列中，这个过程会根据 `Exchange` 的类型、`Exchange` 和队列之间的 `binding` 以及消息的 `routing key` 进行路由   

`Exchange` 的类型有四种:

- Default: 默认类型
- Direct: 直连
- Topic: 主题
- Fanout: 扇形
- Headers: 头信息
- Dead Letter: 捕获无法传递的消息

#### Default

默认交换机（default exchange）实际上是一个由消息代理预先声明好的没有名字（名字为空字符串）的直连交换机（direct exchange）

 特点: 所有新建的队列都会自动绑定至 Default Exchange, 绑定的路由键（routing key）名称与队列名称相同

路由规则: 消息的 routing key == 队列的 routing key, 由于此时队列的名字与队列的 routing key 相同, 看起来就像是交换机直接把消息分发到队列上

#### Direct

特点: 新建的队列绑定到交换机时, 会被赋予一个 binding key

路由规则: 消息的 routing key == 队列的 binding key

#### Topic

主题交换机, 是属于直练交换机的模糊匹配

路由规则: 使用通配符匹配, 会把消息分发到一个或多个队列上

#### Fanout

扇形交换机, 顾名思义, 会把消息分发到所有绑定的队列上

特点: 忽略 routing key & binding key

路由规则: 当消息发送到扇形交换机, 交换机会把消息分发到绑定的所有队列

#### Header

类似 Topic, 不过不使用 routing key, 而是根据头信息来匹配

#### Dead Letter

捕获所有无法投递（也就是它们无法匹配所有已定义的 Exchange 和队列的 binding 关系）的消息

