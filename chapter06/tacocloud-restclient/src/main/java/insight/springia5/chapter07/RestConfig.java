package insight.springia5.chapter07;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.client.Traverson;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

/**
 * 配置链接 & RestTemplate & Traverson
 * 
 * @author insight 
 * @since 2021/7/20
 */
@Configuration
public class RestConfig {
    @Value("${chapter07.rest.ip}")
    public String restUrl;
    @Value("${chapter07.traverson.ip}")
    public String traversonUrl;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public Traverson traverson() {
        return new Traverson(
                URI.create(traversonUrl),
                MediaTypes.HAL_JSON
        );
    }
}