package insight.springia5.chapter07;

import insight.springia5.chapter06.domain.Taco;
import insight.springia5.chapter06.domain.TacoIngredient;
import insight.springia5.chapter06.domain.TacoIngredient.Type;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.hateoas.client.Traverson;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 调用 RestClient 的方法 消费服务
 * CommandLineRunner 项目启动时自动运行的任务
 *
 * @author insight
 * @since 2021/7/20
 */
@Component
@Slf4j
public class ConsumeRest {
    private RestClient restClient;
    private Traverson traverson;

    @Autowired
    public ConsumeRest(RestClient restClient, Traverson traverson) {
        this.restClient = restClient;
        this.traverson = traverson;
    }

    /* Use RestClient */

    // @Bean
    public CommandLineRunner fetchIngredients() {
        return args -> {
            log.info("----------------------- GET -------------------------");
            log.info("GETTING INGREDIENT BY IDE");
            log.info("TacoIngredient:  " + restClient.getIngredientById("CHED"));
            log.info("GETTING ALL INGREDIENTS");
            List<TacoIngredient> ingredients = restClient.getIngredients();
            log.info("All ingredients:");
            for (TacoIngredient ingredient : ingredients) {
                log.info("   - " + ingredient);
            }
        };
    }

    // @Bean
    public CommandLineRunner putAnIngredient() {
        return args -> {
            log.info("----------------------- PUT -------------------------");
            TacoIngredient before = restClient.getIngredientById("LETC");
            log.info("BEFORE:  " + before);
            restClient.updateIngredient(new TacoIngredient("LETC", "Shredded " +
                    "Lettuce", Type.VEGGIES));
            TacoIngredient after = restClient.getIngredientById("LETC");
            log.info("AFTER:  " + after);
        };
    }

    // @Bean
    public CommandLineRunner addAnIngredient() {
        return args -> {
            log.info("----------------------- POST -------------------------");
            TacoIngredient chix = new TacoIngredient("CHIX", "Shredded " +
                    "Chicken", Type.PROTEIN);
            TacoIngredient chixAfter = restClient.createIngredient(chix);
            log.info("AFTER=1:  " + chixAfter);
        };
    }


    // @Bean
    public CommandLineRunner deleteAnIngredient() {
        return args -> {
            log.info("----------------------- DELETE " +
                    "-------------------------");
            // start by adding a few ingredients so that we can delete them 
            // later...
            TacoIngredient beefFajita = new TacoIngredient("BFFJ", "Beef " +
                    "Fajita", Type.PROTEIN);
            restClient.createIngredient(beefFajita);
            TacoIngredient shrimp = new TacoIngredient("SHMP", "Shrimp",
                    Type.PROTEIN);
            restClient.createIngredient(shrimp);


            TacoIngredient before = restClient.getIngredientById("CHIX");
            log.info("BEFORE:  " + before);
            restClient.deleteIngredient(before);
            TacoIngredient after = restClient.getIngredientById("CHIX");
            log.info("AFTER:  " + after);
            before = restClient.getIngredientById("BFFJ");
            log.info("BEFORE:  " + before);
            restClient.deleteIngredient(before);
            after = restClient.getIngredientById("BFFJ");
            log.info("AFTER:  " + after);
            before = restClient.getIngredientById("SHMP");
            log.info("BEFORE:  " + before);
            restClient.deleteIngredient(before);
            after = restClient.getIngredientById("SHMP");
            log.info("AFTER:  " + after);
        };
    }

    /* Use Traverson */

    @Bean
    public CommandLineRunner traversonGetIngredients() {
        return args -> {
            Iterable<TacoIngredient> ingredients = restClient.getIngredientsByTraverson();
            log.info("----------------------- GET INGREDIENTS WITH TRAVERSON -------------------------");
            for (TacoIngredient ingredient : ingredients) {
                log.info("   -  " + ingredient);
            }
        };
    }

    @Bean
    public CommandLineRunner traversonRecentTacos() {
        return args -> {
            Iterable<Taco> recentTacos = restClient.getRecentTacosWithTraverson();
            log.info("----------------------- GET RECENT TACOS WITH TRAVERSON -------------------------");
            for (Taco taco : recentTacos) {
                log.info("   -  " + taco);
            }
        };
    }
}