package insight.springia5.chapter07;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

/**
 * 使用 data-rest
 * 
 * @author insight 
 * @since 2021/7/20
 */
@SpringBootApplication
@EntityScan("insight.springia5.chapter06.domain")
public class TacoCloudRestApplication {
    public static void main(String[] args) {
        SpringApplication.run(TacoCloudRestApplication.class, args);
    }
}