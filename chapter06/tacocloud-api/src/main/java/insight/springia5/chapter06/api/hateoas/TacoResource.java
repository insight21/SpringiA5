package insight.springia5.chapter06.api.hateoas;

import insight.springia5.chapter06.domain.Taco;
import lombok.Getter;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.RepresentationModel;

import java.util.Date;

/**
 * TacoResource 携带链接的 Taco 对象
 * 继承 RepresentationModel 有 Link 对象列表和管理列表的方法
 *
 * 编写 IngredientResource 后 引入装配器
 *
 * @author insight 
 * @since 2021/7/19
 */
@Getter
public class TacoResource extends RepresentationModel<TacoResource> {
    private static final IngredientResourceAssembler
            INGREDIENT_RESOURCE_ASSEMBLER = new IngredientResourceAssembler();

    private String name;
    private Date createdAt;
    private CollectionModel<IngredientResource> ingredients;

    public TacoResource(Taco taco) {
        this.name = taco.getName();
        this.createdAt = taco.getCreatedAt();
        this.ingredients =
                INGREDIENT_RESOURCE_ASSEMBLER.toCollectionModel(taco.getIngredients());
    }
}