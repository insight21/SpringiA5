package insight.springia5.chapter06.api.controller;

import insight.springia5.chapter06.data.IngredientRepository;
import insight.springia5.chapter06.domain.TacoIngredient;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Optional;

/**
 * 
 * 
 * @author insight 
 * @since 2021/7/17
 */
@Api(tags = "Taco 配料")
@RestController
@RequestMapping(path="/ingredients", produces="application/json")
@CrossOrigin
public class IngredientController {

    private IngredientRepository repo;

    @Autowired
    public IngredientController(IngredientRepository repo) {
        this.repo = repo;
    }

    @ApiOperation("获取所有 Taco 配料")
    @GetMapping
    public Iterable<TacoIngredient> allIngredients() {
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public Optional<TacoIngredient> byId(@PathVariable String id) {
        return repo.findById(id);
    }

    @PutMapping("/{id}")
    public void updateIngredient(@PathVariable String id, @RequestBody TacoIngredient ingredient) {
        if (!ingredient.getId().equals(id)) {
            throw new IllegalStateException("Given ingredient's ID doesn't match the ID in the path.");
        }
        repo.save(ingredient);
    }

    @PostMapping
    public ResponseEntity<TacoIngredient> postIngredient(@RequestBody TacoIngredient ingredient) {
        TacoIngredient saved = repo.save(ingredient);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(URI.create("http://localhost:8080/ingredients/" + ingredient.getId()));
        return new ResponseEntity<>(saved, headers, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public void deleteIngredient(@PathVariable String id) {
        repo.deleteById(id);
    }
}
