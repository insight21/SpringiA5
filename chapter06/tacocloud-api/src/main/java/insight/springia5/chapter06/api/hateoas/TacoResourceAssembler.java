package insight.springia5.chapter06.api.hateoas;

import insight.springia5.chapter06.api.controller.DesignController;
import insight.springia5.chapter06.domain.Taco;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

/**
 * 将 Taco 转换成 TacoResource 的资源装配器
 * 
 * @author insight 
 * @since 2021/7/19
 */
public class TacoResourceAssembler extends RepresentationModelAssemblerSupport<Taco, TacoResource> {

    public TacoResourceAssembler() {
        super(DesignController.class, TacoResource.class);
    }

    /**
     * 转换成 TacoResource
     */
    @Override
    protected TacoResource instantiateModel(Taco taco) {
        return new TacoResource(taco);
    }

    /**
     * 填充链接
     */
    @Override
    public TacoResource toModel(Taco taco) {
        return createModelWithId(taco.getId(), taco);
    }
}