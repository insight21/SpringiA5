package insight.springia5.chapter06.api.hateoas;

import insight.springia5.chapter06.api.controller.IngredientController;
import insight.springia5.chapter06.domain.TacoIngredient;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

/**
 * 将 Taco 转换成 TacoResource 的资源装配器
 * 
 * @author insight 
 * @since 2021/7/19
 */
public class IngredientResourceAssembler extends RepresentationModelAssemblerSupport<TacoIngredient, IngredientResource> {

    public IngredientResourceAssembler() {
        super(IngredientController.class, IngredientResource.class);
    }

    /**
     * 转换成 TacoResource
     */
    @Override
    protected IngredientResource instantiateModel(TacoIngredient ingredient) {
        return new IngredientResource(ingredient);
    }

    /**
     * 填充链接
     */
    @Override
    public IngredientResource toModel(TacoIngredient ingredient) {
        return createModelWithId(ingredient.getId(), ingredient);
    }
}