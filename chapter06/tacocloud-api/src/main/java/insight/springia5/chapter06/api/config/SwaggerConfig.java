package insight.springia5.chapter06.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

/**
 * 
 * 
 * @author insight 
 * @since 2021/7/17
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    private final String apiPackage = "insight.springia5.chapter06.api.controller";

    @Bean
    public Docket createRestApi(Environment environment) {
        Profiles profiles = Profiles.of("!prod");
        boolean b = environment.acceptsProfiles(profiles);

        return new Docket(DocumentationType.SWAGGER_2)
                .enable(b)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage(apiPackage))
                .paths(PathSelectors.any())
                .build();
    }


    @Bean
    protected ApiInfo apiInfo() {
        return new ApiInfo(
                "SpringiA5 第六章接口文档",
                "TacoCloud",
                "1.0",
                "",
                new Contact("insight21", "insight21.gitee.io", ""),
                "",
                "",
                new ArrayList<>()
        );
    }
}