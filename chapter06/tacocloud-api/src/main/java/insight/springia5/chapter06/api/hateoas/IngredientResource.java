package insight.springia5.chapter06.api.hateoas;

import insight.springia5.chapter06.domain.TacoIngredient;
import insight.springia5.chapter06.domain.TacoIngredient.Type;
import lombok.Getter;
import org.springframework.hateoas.RepresentationModel;

/**
 * TacoResource 携带链接的 Taco 对象
 * 继承 RepresentationModel 有 Link 对象列表和管理列表的方法
 *
 * @author insight 
 * @since 2021/7/19
 */
@Getter
public class IngredientResource extends RepresentationModel<IngredientResource> {

    private String name;
    private Type type;

    public IngredientResource(TacoIngredient ingredient) {
        this.name = ingredient.getName();
        this.type = ingredient.getType();
    }
}