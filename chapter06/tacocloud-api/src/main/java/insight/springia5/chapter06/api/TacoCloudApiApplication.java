package insight.springia5.chapter06.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 *
 *
 * @author insight
 * @since 2021/7/17
 */
@SpringBootApplication
@EnableJpaRepositories(basePackages = "insight.springia5.chapter06.data")
@EntityScan("insight.springia5.chapter06.domain")
public class TacoCloudApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(TacoCloudApiApplication.class, args);
    }
}