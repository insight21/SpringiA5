package insight.springia5.chapter06.api.controller;

import insight.springia5.chapter06.data.OrderRepository;
import insight.springia5.chapter06.domain.Order;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * @author insight
 * @since 2021/7/17
 */
@Api(tags = "订单处理")
@RestController
@RequestMapping(path = "/orders",
        produces = "application/json")
@CrossOrigin
public class OrderController {
    private OrderRepository orderRepository;

    @Autowired
    public OrderController(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @ApiOperation("查看所有订单")
    @GetMapping(produces="application/json")
    public Iterable<Order> allOrders() {
        return orderRepository.findAll();
    }

    @ApiOperation("提交订单")
    @PostMapping(consumes="application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public Order postOrder(@RequestBody Order order) {
        return orderRepository.save(order);
    }

    @ApiOperation("替换订单")
    @PutMapping(path="/{orderId}", consumes="application/json")
    public Order putOrder(@RequestBody Order order) {
        return orderRepository.save(order);
    }

    /**
     * 修改订单信息
     *
     * PUT & PATCH 的语义
     * 1. PUT 大规模替换
     * 2. PATCH 局部更新
     */
    @ApiOperation("替换订单")
    @PatchMapping(path = "/{orderId}", consumes = "application/json")
    public Order patchOrder(@PathVariable("orderId") long id,
                            @RequestBody Order patch) {
        Order order = orderRepository.findById(id).get();

        if (patch.getDeliveryName() != null) {
            order.setDeliveryName(patch.getDeliveryName());
        }
        if (patch.getDeliveryStreet() != null) {
            order.setDeliveryStreet(patch.getDeliveryStreet());
        }
        if (patch.getDeliveryCity() != null) {
            order.setDeliveryCity(patch.getDeliveryCity());
        }
        if (patch.getDeliveryState() != null) {
            order.setDeliveryState(patch.getDeliveryState());
        }
        if (patch.getDeliveryZip() != null) {
            order.setDeliveryZip(patch.getDeliveryState());
        }
        if (patch.getCcNumber() != null) {
            order.setCcNumber(patch.getCcNumber());
        }
        if (patch.getCcExpiration() != null) {
            order.setCcExpiration(patch.getCcExpiration());
        }
        if (patch.getCcCVV() != null) {
            order.setCcCVV(patch.getCcCVV());
        }
        return orderRepository.save(order);
    }

    /**
     * 订单不存在 抛出异常 不做处理
     * 删除不会返回内容 使用 204 暗示删除成功
     */
    @ApiOperation("删除订单")
    @DeleteMapping("/{orderId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOrder(@PathVariable("orderId") long id) {
        try {
            orderRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
        }
    }
}