package insight.springia5.chapter06.tacocloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * 
 * @author insight 
 * @since 2021/7/17
 */
@SpringBootApplication(scanBasePackages = "insight.springia5.chapter06.api")
public class TacoCloudApplication {
    public static void main(String[] args) {
        SpringApplication.run(TacoCloudApplication.class, args);
    }
}