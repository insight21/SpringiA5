- [第6章 搭建一个 SpringBoot Web Restful 应用](#第6章-搭建一个-springboot-web-restful-应用)
  - [前端部分](#前端部分)
  - [后端部分](#后端部分)
    - [预备工作](#预备工作)
    - [实体类](#实体类)
    - [数据访问](#数据访问)
    - [接口](#接口)

# 第6章 搭建一个 SpringBoot Web Restful 应用

## 前端部分
SpringiA5 使用的是 `Angular` 框架，因此需要通过 `npm` 先安装 `@angular/cli@1.5.0`     
然后把 `tacocloud-ui` 拷贝到自己的项目中，在 `tacocloud-ui` 目录下运行命令安装项目依赖和启动项目
```bash
npm install
ng serve
```
前端的链接为: `http://localhost:4200/`

注意：SpringiA5 的第6、7章的前端，只编写了获取原料、设计、查看最近设计，和购物车的接口

注意：SpringiA5 中使用到了大量的 `JPA` 操作，但实际工作中很少使用

## 后端部分
从这一部分开始，就要开始安装规范来编写项目了，项目可分为以下几个部分来编写
- `tacocloud-api` : 前后端交互接口，大部分为 `Controller`
- `tacocloud-data` : 数据访问层，也就是 `Dao` 层
- `tacocloud-domain` : 各种实体类
- `tacocloud` : 项目的启动入口

### 预备工作
这部分的内容，主要是把前端中使用到的接口整理出来，方便下一步的编写   
初步整理好的接口文档放在 `chapter06` 目录下的 `api-doc.md`     
`Swagger` 在线地址: http://localhost:8080/doc.html      
有普通版和增强版, 增强版为 `swagger-bootstrap-ui`, 按个人喜好使用
(~~TODO~~: 使用 `Swagger` 把接口文档整理成在线 `html` 和 离线 `markdown` )   

### 实体类
`tacocloud-domain` 中需要编写以下几个实体类，以及引进 `lombok`     
直接从上一章复制过来
- 用户
- Taco，在6.3小节引入 `spring-boot-data-rest` 自动生成增删改查
- TacoIngredient
- 订单

### 数据访问
先从上一章直接复制过来，编写 `controller` 的时候，需要用到什么再添加

注意：`TacoRepository` 从 `CrudRepository` 修改成 `PagingAndSortingRepository`

### 接口
从这一部分开始，根据接口文档，跟随6.1小节学习编写 `RESTful` 控制器，处理增删改查请求，详见代码

**注意**：多模块工程下，调用其他模块的 `bean` 需要手动添加包，具体有两种方式
一开始使用第一种有效，后来重新打开 `idea` 后失效，改为第二种重新生效（玄学编程）
```java
@SpringBootApplication(scanBasePackages = "xxx.xxx")
```
或者
```java
@EnableJpaRepositories(basePackages = "xxx.xxx")
@EntityScan("xxx.xxx")
```
