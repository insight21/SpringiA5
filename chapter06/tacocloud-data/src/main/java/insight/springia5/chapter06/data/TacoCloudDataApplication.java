package insight.springia5.chapter06.data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

/**
 * 使用 data-rest
 * 
 * @author insight 
 * @since 2021/7/19
 */
@SpringBootApplication
@EntityScan("insight.springia5.chapter06.domain")
public class TacoCloudDataApplication {
    public static void main(String[] args) {
        SpringApplication.run(TacoCloudDataApplication.class, args);
    }
}