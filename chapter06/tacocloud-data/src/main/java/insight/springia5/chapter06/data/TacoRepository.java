package insight.springia5.chapter06.data;

import insight.springia5.chapter06.domain.Taco;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * 使用 @RepositoryRestResource 调整路径
 */
@RepositoryRestResource(path = "tacos", itemResourceRel = "taco", collectionResourceRel = "tacos")
public interface TacoRepository
         extends PagingAndSortingRepository<Taco, Long> {

}
