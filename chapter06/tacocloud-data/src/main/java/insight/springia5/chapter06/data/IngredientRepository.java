package insight.springia5.chapter06.data;

import insight.springia5.chapter06.domain.TacoIngredient;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "ingredients", itemResourceRel = "ingredient",
        collectionResourceRel = "ingredients")
public interface IngredientRepository 
         extends CrudRepository<TacoIngredient, String> {

}
