package insight.springia5.chapter06.data;

import insight.springia5.chapter06.domain.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository 
         extends CrudRepository<Order, Long> {

}
