package insight.springia5.chapter06.data;

import insight.springia5.chapter06.domain.User;
import org.springframework.data.repository.CrudRepository;

/**
 * @author insight
 * @since 2021/5/9
 */
public interface UserRepository extends CrudRepository<User, Long> {
    /**
     * 根据用户名查找用户 用于登录
     *
     * @param username 用户名
     * @return 用户
     */
    User findByUsername(String username);
}