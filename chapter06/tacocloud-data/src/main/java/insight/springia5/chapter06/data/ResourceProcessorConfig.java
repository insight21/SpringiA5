package insight.springia5.chapter06.data;

import insight.springia5.chapter06.domain.Taco;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelProcessor;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * 添加自定义超链接
 * 书上的过时了 正确写法参考官网
 * https://docs.spring.io/spring-hateoas/docs/current/reference/html/#server.processors
 * 
 * @author insight 
 * @since 2021/7/19
 */
@Configuration
public class ResourceProcessorConfig {
    private static class ResourceProcessor implements RepresentationModelProcessor<CollectionModel<EntityModel<Taco>>> {

        @Override
        public CollectionModel<EntityModel<Taco>> process(CollectionModel<EntityModel<Taco>> model) {
            model.add(
                    linkTo(methodOn(RecentController.class).recentTacos())
                            .withRel("recents")
            );
            return model;
        }
    }

    @Bean
    public ResourceProcessor processor() {
        return new ResourceProcessor();
    }
}