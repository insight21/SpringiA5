- [第6章 接口文档](#第6章-接口文档)
    - [查看最近设计的 Taco](#查看最近设计的-taco)
    - [设计 Taco](#设计-taco)
      - [获取配料](#获取配料)
      - [提交设计好的 Taco](#提交设计好的-taco)
    - [提交订单](#提交订单)

# 第6章 接口文档

### 查看最近设计的 Taco
|||
|---|---|
| Method | Get |
| url | http://localhost:8080/design/recent |

### 设计 Taco

#### 获取配料
|||
|---|---|
| Method | Get |
| url | http://localhost:8080/ingredientsx |

#### 提交设计好的 Taco
|||
|---|---|
| Method | Post |
| url | http://localhost:8080/design |

### 提交订单
|||
|---|---|
| Method | Post |
| url | http://localhost:8080/orders |